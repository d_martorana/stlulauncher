﻿Imports System.IO

Public Class Form1
    Public Setup As New INIFile(Application.StartupPath & "\Setup.ini")
    Public LanguageFile As New INIFile(Application.StartupPath & "\language.ini")
    Public language As String = Setup.ReadValue("Configuration", "language")
    Public pgrcmdFolder As String = Setup.ReadValue("Configuration", "pgrcmdFolder")
    Public binFolder As String = Setup.ReadValue("FileBin", "Folder")
    Public nomeFile(10) As String
    Public address(10) As String
    Public stringaFile As String = ""
    Public codicePiastra As String = ""
    Public esito(10) As String
    Public esitoIndice As Integer = 0
    Public ultimoFile As Integer = 0
    Public esitoFinale As String = "KO"

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        picSfondo.BackColor = Color.FromArgb(&HFF55166E)
        picLogo.BackColor = Color.FromArgb(&HFF55166E)
        Me.BackColor = Color.LightGray
        Me.Text = Messaggio(0) & " - " & Application.ProductVersion
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        lblMessage.Text = Messaggio(8)
        Dim parametri(2) As String
        parametri(1) = "-c SWD UR -ME"
        parametri(2) = " -V -Rst"
        Application.DoEvents()
        lstShell.Items.Clear()
        btnNuova.Enabled = False
        btnStart.Enabled = False
        For i = 1 To 10
            esito(i) = "KO"
        Next
        esitoIndice = 0
        If pgrcmdFolder.Substring(pgrcmdFolder.Length - 1, 1) <> "\" Then
            pgrcmdFolder &= "\"
        End If


        Dim oProcess As New Process()

        Dim oStartInfo As New ProcessStartInfo(pgrcmdFolder & "ST-LINK_CLI.exe", parametri(1) & stringaFile & parametri(2))

        oStartInfo.UseShellExecute = False
        oStartInfo.RedirectStandardOutput = True
        oStartInfo.WindowStyle = ProcessWindowStyle.Maximized
        oProcess.StartInfo = oStartInfo
        oProcess.StartInfo.CreateNoWindow = True
        Try
            Application.DoEvents()
            oProcess.Start()
            Dim sOutput As String
            Using oStreamReader As System.IO.StreamReader = oProcess.StandardOutput
                sOutput = " "
                Do Until sOutput Is Nothing
                    lstShell.Items.Add(sOutput)
                    sOutput = oStreamReader.ReadLine '.ReadToEnd()
                Loop
            End Using
            For i = 1 To lstShell.Items.Count - 1
                If InStr(lstShell.Items(i), "Verification...OK", CompareMethod.Text) > 0 Then
                    esitoIndice += 1
                    esito(esitoIndice) = "OK"
                End If

            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        For i = 1 To ultimoFile
            If esito(i) = "KO" Then
                esitoFinale = "KO"
                Exit For
            Else
                esitoFinale = "OK"
            End If
        Next
        If esitoFinale = "KO" Then
            lblMessage.Text = Messaggio(20)
            Me.BackColor = Color.Red
        Else
            lblMessage.Text = Messaggio(7)
            Me.BackColor = Color.LawnGreen
        End If
        btnNuova.Enabled = True
        btnReset.Enabled = True
        Me.AcceptButton = btnNuova
        Try
            Directory.SetCurrentDirectory(pgrcmdFolder)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        ScriviLog()

    End Sub

    Sub ScriviLog()
        Dim fileLog As String = Application.StartupPath & "\" & txtSerialNumber.Text.Substring(1, 7) & ".txt"

        If Not File.Exists(fileLog) Then
            Using newFileLog As StreamWriter = File.CreateText(fileLog)
                newFileLog.WriteLine(Messaggio(10))
            End Using
        End If

        Using log As StreamWriter = File.AppendText(fileLog)
            log.WriteLine(txtSerialNumber.Text & "," & Now.ToShortDateString & "," & Now.ToShortTimeString & "," & txtOperator.Text & "," & esitofinale)
        End Using

    End Sub
    Private Sub txtOperator_TextChanged(sender As Object, e As EventArgs) Handles txtOperator.TextChanged

    End Sub
    Private Sub txtOperator_KeyUp(sender As Object, e As KeyEventArgs) Handles txtOperator.KeyUp
        If txtOperator.Text.Length > 0 Then
            If e.KeyCode = 13 Then
                txtOperator.Enabled = False
                txtSerialNumber.Enabled = True
                txtSerialNumber.Focus()
                lblMessage.Text = Messaggio(2)
            End If
        End If
    End Sub

    Private Sub CaricaParametri()
        For i = 1 To 10
            nomeFile(i) = Setup.ReadValue(codicePiastra, "FileName" & i)
            address(i) = Setup.ReadValue(codicePiastra, "Address" & i)
        Next
    End Sub

    Private Sub txtSerialNumber_TextChanged(sender As Object, e As EventArgs) Handles txtSerialNumber.TextChanged

    End Sub
    Private Sub txtSerialNumber_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSerialNumber.KeyUp
        If txtSerialNumber.Text.Length > 0 Then
            If e.KeyCode = 13 Then
                If txtSerialNumber.Text.Length = 16 Then
                    lstNomeFile.Items.Clear()
                    ultimoFile = 0
                    stringaFile = ""
                    codicePiastra = txtSerialNumber.Text.Substring(1, 7)
                    Dim Folder As String = Setup.ReadValue(codicePiastra, "FileFolder")
                    If Folder <> "Failed" And Folder <> "" Then
                        binFolder = Folder
                    End If
                    If binFolder.Substring(binFolder.Length - 1, 1) <> "\" Then
                        binFolder &= "\"
                    End If
                    CaricaParametri()
                    If nomeFile(1) = "Failed" Then
                        btnNuova.Enabled = True
                        txtSerialNumber.Enabled = False
                        Me.AcceptButton = btnNuova
                        btnReset.Enabled = True
                        lblMessage.Text = Messaggio(21)
                        Me.BackColor = Color.Red
                    Else
                        For i = 1 To 10
                            If nomeFile(i) <> "Failed" Then
                                ultimoFile = i
                            End If
                        Next
                        For i = 1 To ultimoFile
                            stringaFile &= " -P """ & binFolder & nomeFile(i) & """ " & address(i)
                            lstNomeFile.Items.Add(nomeFile(i))
                        Next
                        lblMessage.Text = (Messaggio(9))
                        txtSerialNumber.Enabled = False
                        btnStart.Enabled = True
                        btnNuova.Enabled = True
                        Me.AcceptButton = btnStart
                    End If
                Else
                    btnNuova.Enabled = True
                    txtSerialNumber.Enabled = False
                    Me.AcceptButton = btnNuova
                    btnReset.Enabled = True
                    lblMessage.Text = Messaggio(22)
                    Me.BackColor = Color.Yellow
                End If
            End If
        End If
    End Sub

    Private Sub Form1_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        txtOperator.Focus()
        lblMessage.Text = Messaggio(1)
        lblOperator.Text = Messaggio(4)
        lblSerialNumber.Text = Messaggio(5)
        lblNomeFile.Text = Messaggio(6)
    End Sub

    Public Function Messaggio(ByVal indiceMessaggio As Integer) As String
        Messaggio = LanguageFile.ReadValue(language.ToString, "msg" & indiceMessaggio)
    End Function

    Private Sub btnNuova_Click(sender As Object, e As EventArgs) Handles btnNuova.Click
        pgrcmdFolder = Setup.ReadValue("Configuration", "pgrcmdFolder")
        binFolder = Setup.ReadValue("FileBin", "Folder")
        lstNomeFile.Items.Clear()
        txtSerialNumber.Text = ""
        txtSerialNumber.Enabled = True
        txtSerialNumber.Focus()
        Me.BackColor = Color.LightGray
        btnNuova.Enabled = False
        btnReset.Enabled = False
        btnStart.Enabled = False
        lblMessage.Text = Messaggio(2)
        lstShell.Items.Clear()
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtOperator.Text = ""
        txtOperator.Enabled = True
        txtOperator.Focus()
        lstNomeFile.Items.Clear()
        txtSerialNumber.Text = ""
        Me.BackColor = Color.LightGray
        btnNuova.Enabled = False
        btnReset.Enabled = False
        lblMessage.Text = Messaggio(1)
        lstShell.Items.Clear()
    End Sub


End Class
